# Ezo Challenge - OOP Calculator

This repository contains the code I produced for the Ezo Recruitment Challenge. The Oriented Object Calculator is implemented in JavaScript.
The link to Ezo's challenge is at : https://github.com/EzoQC/DefiCode

The website is live at : https://mathis.chapuis02.gitlab.io/ezo-challenge---oop-calculator

It can handle the following instructions

* All priorities with parenthesises
* Additions
* Substractions
* Divisions
* Multiplications
* Exponents
* Square roots using "sqrt()"
* Sinus using "sin()"
* Cosinus using "cos()"
* Tangeants using "tan()"

To calculate the value of a calculation, a tree is created in the Reversed Polish Notation, using the Shunting Yard Algorithm.