/**
 * Listener on the window that triggers when when press enter or the button 'Calculate'.
 */

window.addEventListener("load", e => {
    const button = document.querySelector("form [type=submit]");
    const resultDiv = document.getElementById("result");
    button.addEventListener("click", e => {
        let expressionForm = document.querySelector("form [type = text]");
        e.preventDefault();
        let result;
        try {
            result = createTree(expressionForm.value).calculate();
        } catch (error) {
            result = error.message;
        }
        
        const el = document.createElement("p");
        el.innerHTML = expressionForm.value+" = "+result;
        expressionForm.value = "";
        resultDiv.appendChild(el);
    })
})