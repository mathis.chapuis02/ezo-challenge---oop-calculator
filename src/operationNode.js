/**
 * Class for all nodes that represent an operation
 */

class OperationNode extends Node {
    constructor (operation)
    {
        super()
        this.operation = operation;
    }
}