/**
 * Node that represents an exponent. First child's value exponent second child's value
 * is calculated with the method 'calculate()'. Even though it is possible to calculate
 * the value while having a non integer exponent using 'Math.pow()', an error is returned
 * the second child's value is not an integer. 
 */

 class ExponentNode extends BinaryNode {
    constructor (firstChild, secondChild)
    {
        super("^", firstChild, secondChild);
    }

    calculate ()
    {
        if (!Number.isInteger(this.secondChild.calculate()))
        {
            throw new TypeError ("Impossible to calculate : non integer exponent");
        }
        if (this.secondChild.calculate() == 0)
        {
            return 1;
        }
        return Math.pow(this.firstChild.calculate(), this.secondChild.calculate());
    }
}