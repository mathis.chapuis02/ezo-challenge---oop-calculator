/**
 * Node that represents an multiplication. To calculate its value, the first child value using its method
 * calculate() is multiplied to the one of the second child.
 */

class MultiplicationNode extends BinaryNode {
    constructor (firstChild, secondChild)
    {
        super("*", firstChild, secondChild);
    }

    calculate ()
    {
        return this.firstChild.calculate()*this.secondChild.calculate();
    }
}