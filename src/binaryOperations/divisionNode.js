/**
 * Node that represents an division. To calculate its value, the first child value using its method
 * calculate() is divided by the one of the second child. If the second child's value is 0, an
 * error is returned.
 */

class DivisionNode extends BinaryNode {
    constructor (firstChild, secondChild)
    {
        super("/", firstChild, secondChild);
    }

    calculate ()
    {
        if (this.secondChild.calculate() === 0)
        {
            throw new TypeError ("Impossible to calculate : division per 0");
        }
        return this.firstChild.calculate()/this.secondChild.calculate();
    }
}