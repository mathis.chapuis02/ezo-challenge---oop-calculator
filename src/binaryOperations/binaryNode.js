/**
 * Abstract class BinaryNode to implement operations such as additions, substractions, or
 * any operation that concern two numbers.
 */

class BinaryNode extends OperationNode {
    constructor (operation, firstChild, secondChild)
    {
        super (operation);
        this.firstChild = firstChild;
        this.secondChild = secondChild;
    }
}