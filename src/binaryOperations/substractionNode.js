/**
 * Node that represents an substraction. To calculate its value, the first child value using its method
 * calculate() is substracted to the one of the second child.
 */

class SubstractionNode extends BinaryNode {
    constructor (firstChild, secondChild)
    {
        super("-", firstChild, secondChild);
    }

    calculate ()
    {
        return this.firstChild.calculate()-this.secondChild.calculate();
    }
}