/**
 * Node that represents an addition. To calculate its value, the first child value using its method
 * calculate() is added to the one of the second child.
 */

class AdditionNode extends BinaryNode {
    constructor (firstChild, secondChild)
    {
        super("+", firstChild, secondChild);
    }

    calculate ()
    {
        return this.firstChild.calculate()+this.secondChild.calculate();
    }
}