/**
 * Class SinNode that calculates the value of the sinus on the child's value using Math.sin()
 */

 class SinNode extends UnitaryNode {
    constructor (child)
    {
        super("sin", child);
    }

    calculate ()
    {
        return Math.sin(this.child.calculate());
    }
}