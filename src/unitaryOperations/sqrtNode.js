/**
 * Class TanNode that calculates the value of the square root on the child's value
 * using Math.sqrt(). Throws a proper error if the value of the child is negative.
 */

class SqrtNode extends UnitaryNode {
    constructor (child)
    {
        super("sqrt", child);
    }

    calculate ()
    {
        if (this.child.calculate() < 0)
        {
            throw new TypeError ("Impossible to calculate : no square root for negative number");
        }
        return Math.sqrt(this.child.calculate());
    }
}