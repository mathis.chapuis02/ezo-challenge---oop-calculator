/**
 * Abstract class UnitaryNode to implement unitary operations such as square roots, sinus, etc...
 */

class UnitaryNode extends OperationNode {
    constructor (operation, child)
    {
        super (operation);
        this.child = child;
    }
}