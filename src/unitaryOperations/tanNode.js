/**
 * Class TanNode that calculates the value of the tangeant on the child's value using Math.tan()
 */

class TanNode extends UnitaryNode {
    constructor (child)
    {
        super("tan", child);
    }

    calculate ()
    {
        return Math.tan(this.child.calculate());
    }
}