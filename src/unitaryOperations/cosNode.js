/**
 * Class CosNode that calculates the value of the cosinus on the child's value using Math.cos()
 */

 class CosNode extends UnitaryNode {
    constructor (child)
    {
        super("cos", child);
    }

    calculate ()
    {
        return Math.cos(this.child.calculate());
    }
}