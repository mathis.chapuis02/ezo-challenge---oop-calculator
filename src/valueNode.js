/**
 * The ValueNode represents the leafs in the tree that will be created.
 * They implement the Node's method 'calculate()' that simply returns the node's value.
 */

class ValueNode extends Node {
    constructor (value)
    {
        super();
        this.value = value;
    }

    calculate ()
    {
        return this.value;
    }
}