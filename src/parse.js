/**
 * In this file are written all the functions necessary to create from a mathematical string
 * expression a tree. The expression is first tokenize into a tab to separated numbers from letters,
 * parenthesises, etc... The Reversed Polish Notation is then created from the tab, and then
 * the tree is created from it.
 */

 // Tab that defines simple binary operations and their level of priority
var priority = {
    "+": 1,
    "-": 1,
    "*": 2,
    "/": 2,
    "^": 3
}

// Tab of the unitary functions implemented

var func = ["cos", "tan", "sin", "sqrt"];

// Create an array of the operators in the hash map "priority"

var operators = Object.keys(priority);

// Defines if an operation is leftly or rightly associated. This notion is used in the shunting
// yard algorithm to create the reversed polish notation

var leftAssociation = {
    "+": true,
    "-": true,
    "*": true,
    "/": true,
    "^": false
}

// Tab of all the letters

var letters=[];
for(var i='A'.charCodeAt(0); i<='Z'.charCodeAt(0); i++) {
    letters.push(String.fromCharCode(i));
    letters.push(String.fromCharCode(i+32));
}

/**
 * 
 * @param expression : a mathematical expression that needs to be evaluated
 * It returns the corresponding Reversed Polish Notation using the shunting yard algorithm
 * For more informations on that algorithm, see the pseudo-code in the following link :
 * https://en.wikipedia.org/wiki/Shunting-yard_algorithm#The_algorithm_in_detail
 */

function createReversePolishNotation (expression)
{
    let queue = [];
    let stack = [];

    stack.top=function top() {
        return this[this.length-1];
    };


    for (let sub of tokenizeExpression(expression))
    {
        if (typeof(sub) == "number")
        {
            queue.push(sub);
        }
        else if (func.includes(sub))
        {
            stack.push(sub);
        }
        else if (sub in priority)
        {
            while (
                stack.length &&
                (
                    func.includes(stack.top()) ||
                    priority[stack.top()] > priority[sub] ||
                    (
                        priority[stack.top()] == priority[sub] &&
                        leftAssociation[stack.top()]
                    )
                ) &&
                stack.top() != "("
            )
            {
                queue.push(stack.pop());
            }
            stack.push(sub);
        }
        else if (sub == '(')
        {
            stack.push(sub);
        }

        else if (sub == ')')
        {
            while (stack.top() != '(')
            {
                queue.push(stack.pop());
            }

            if (stack.top() === '(')
            {
                stack.pop();
            }
        }

        else {
            queue.push(sub);
        }
    }

    while (stack.length)
    {
        queue.push(stack.pop());
    }

    return queue;
}

/**
 * 
 * @param tokenizedExpression : the mathematical expression tokenized in a tab
 * It adds a 0 if the symbol "-" is preceded by a "(" or any operators, meaning the "-"
 * is meant to calculate the negative value of the next node, and not a susbtractions.
 * A 0 is added before such a unitary "-". The unitary "-" is then evaluated as a
 * SubstractionNode(0,number)
 */

function addUnitarySubstractions (tokenizedExpression)
{
    let i = 0;

    while (i < tokenizedExpression.length)
    {
        if (tokenizedExpression[i] === "-" &&
            (
                [undefined, "("].includes(tokenizedExpression[i-1]) ||
                operators.includes(tokenizedExpression[i-1])
            )
        )
        {
            tokenizedExpression.splice(i, 0, 0);
        }
        i++;
    }
    return tokenizedExpression;
}

/**
 * 
 * @param expression : a mathematical expression
 * Returns the expression tokenized.
 */

function tokenizeExpression (expression)
{
    expression = expression.replace(/\s+/g, "").toLowerCase();
    let temp = "";
    let number = false;
    let func = false;
    let tab = [];

    console.log("Expression : "+expression);

    for (sub of expression)
    {
        if (letters.includes(sub))
        {
            if (number)
            {
                tab.push(+temp);
                temp = "";
                number = false;
            }
            func = true;
            temp += sub;
        }
        else if (!isNaN(+sub) || [",", "."].includes(sub))
        {
            if (func)
            {
                tab.push(temp);
                temp = "";
                func = false;
            }
            number = true;
            temp += sub;
        }
        else
        {
            if (func)
            {
                tab.push(temp);
                temp = "";
                func = false;
            }
            else if (number)
            {
                tab.push(+temp);
                temp = "";
                number = false;
            }
            tab.push(sub);
        }
    }

    if (number)
    {
        tab.push(+temp);
    }
    if (func)
    {
        tab.push(temp);
    }

    return addUnitarySubstractions(tab);
}

/**
 * 
 * @param expression : a mathematical string expression
 * Returns the root of the tree corresponding to the reversed polish notation of the expression
 */

function createTree (expression)
{
    let tab = createReversePolishNotation(expression);
    if (tab.length === 1)
    {
        return new ValueNode(expression[0]);
    }
    while(tab.length != 1)
    {
        let firstOperatorIndex = tab.findIndex(e => {
            return operators.includes(e) || func.includes(e);
        });

        let a = tab[firstOperatorIndex-2];
        let b = tab[firstOperatorIndex-1];
        let operator = tab [firstOperatorIndex];
        if (a === undefined && operator === "-")
        {
            a = 0;
            tab.unshift(a);
            firstOperatorIndex++;
        }
        if (!(a instanceof OperationNode))
        {
            a = new ValueNode (a);
        }
        if (!(b instanceof OperationNode))
        {
            b = new ValueNode (b);
        }

        switch(operator)
        {
            case "+": operator = new AdditionNode (a, b); break;
            case "-": operator = new SubstractionNode (a, b); break;
            case "/": operator = new DivisionNode (a, b); break;
            case "*": operator = new MultiplicationNode (a, b); break;
            case "^": operator = new ExponentNode(a, b); break;
            case "sqrt": operator = new SqrtNode (b); break;
            case "tan": operator = new TanNode(b); break;
            case "sin": operator = new SinNode(b); break;
            case "cos": operator = new CosNode(b); break;
        }

        if (operator instanceof BinaryNode)
        {
            tab.splice(firstOperatorIndex-2, 3, operator);
        }
        else if (operator instanceof UnitaryNode)
        {
            tab.splice(firstOperatorIndex-1, 2, operator);
        }
    }
    
    return tab[0];
}